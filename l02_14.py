#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Universidade Federal do Rio de Janeiro
Pedro Angelo Medeiros Fonini <pedro.fonini@smt.ufrj.br>
Machine Learning - 2017.3

Lista 02, Exercício 14
"""

import os
import argparse
import timeit
import math

import numpy as np
import scipy
from scipy import special
import matplotlib as mpl
if "DISPLAY" not in os.environ:
    mpl.use('Agg')
mpl.rcParams['text.usetex'] = True
mpl.rcParams['text.latex.unicode'] = True
mpl.rcParams['font.family'] = 'serif'
mpl.rcParams['font.size'] = 20
import matplotlib.pyplot as plt

import shelve_helper
import sciutils

def main(sigma, mu, popsize, ellipses, bgres,
         show_plots=False, shelve_title=None, save=True):

    categoria = 'l02_ex14'
    sim_dir = shelve_helper.setup_save_dir(categoria)

    start_time = timeit.default_timer()

    Sigma = np.asarray([float(x) for x in sigma.split(',')]).reshape((2,2))
    mu = np.asarray([float(x) for x in mu.split(',')]).reshape((-1,2))

    K = mu.shape[0]

    classes = np.random.randint(K, size=(popsize,))


    class_colors = np.array([[230, 159, 0],
                             [ 86, 180, 233],
                             [  0, 158, 115]])/255
    class_cm = mpl.colors.LinearSegmentedColormap.from_list(
        'cater', class_colors, N=3)

    ### Ellipse axes
    sciutils.print_raw('Show ellipses ... ')
    start_02 = timeit.default_timer()

    l, U = np.linalg.eigh(Sigma)
    L12 = np.diag(l**(1/2))
    UL12 = U.dot(L12) # U*Lambda^{1/2}

    end_02 = timeit.default_timer()
    print('done in {} seg'.format(end_02 - start_02))

    print('l    = \n{}'.format(l))
    print('U    = \n{}'.format(U))
    print('L12  = \n{}'.format(L12))
    print('UL12 = \n{}'.format(UL12))

    el_sizes = [float(x) for x in ellipses.split(',')]
    num_elps = len(el_sizes)
    colors = plt.cm.viridis(np.linspace(0, 0.8, num_elps))

    fig, axes = plt.subplots(1, 1, squeeze=False, figsize=(15, 6))
    t = np.linspace(0, 2*np.pi, num=1001)
    y = np.stack((np.cos(t), np.sin(t)), axis=1)
    y = y.dot(UL12.T)
    for m in mu:
        for sz, cl in zip(el_sizes, colors):
            y_scaled = m + sz*y
            axes[0,0].plot(y_scaled[:,0], y_scaled[:,1], color=cl, linewidth=2)
    axes[0,0].plot(mu[:,0], mu[:,1], 'x', markersize=10, markeredgewidth=4,
        label=sciutils.latex_formatter.format(r'Class centers'))
    axes[0,0].set_aspect('equal')
    axes[0,0].xaxis.set_major_locator(
        mpl.ticker.MaxNLocator(integer=True))
    axes[0,0].yaxis.set_major_locator(
        mpl.ticker.MaxNLocator(integer=True))
    axes[0,0].legend()
    axes[0,0].grid(True)
    sciutils.plot_finalize(plt, sim_dir / ('contour.eps'), show=show_plots)

    print()

    ### Classify points
    sciutils.print_raw('Classify points ... ')
    start_02 = timeit.default_timer()

    pontos = np.empty((popsize, 2))
    for k, m in enumerate(mu):
        mask = (classes == k)
        pts = np.random.standard_normal((mask.sum(), 2))
        pontos[mask] = m + pts.dot(UL12.T)

    _, ax = plt.subplots(1, 1, squeeze=True, figsize=(15, 6))
    ax.plot(pontos[:,0], pontos[:,1], '.')
    xl = ax.get_xlim()
    yl = ax.get_ylim()
    plt.close('all')

    xy = np.stack(np.meshgrid(np.linspace(xl[0],xl[1],bgres),
                              np.linspace(yl[0],yl[1],bgres)))
    im_posteriori = sciutils.get_gauss_posteriori(xy, mu, Sigma=Sigma, axis=0)
    im_posteriori0 = sciutils.get_gauss_posteriori(xy, mu, axis=0)
    im_posteriori_error = special.logsumexp(im_posteriori, axis=0)
    im_map_class = np.argmax(im_posteriori, axis=0)
    im_map0_class = np.argmax(im_posteriori0, axis=0)

    printvar = sciutils.printvar
    posteriori = sciutils.get_gauss_posteriori(pontos, mu, Sigma=Sigma)
    posteriori0 = sciutils.get_gauss_posteriori(pontos, mu)
    map_class = np.argmax(posteriori, axis=1)
    map0_class = np.argmax(posteriori0, axis=1)
    hits = (map_class == classes)
    hits0 = (map0_class == classes)
    hitrate = hits.mean()
    hitrate0 = hits0.mean()
    confusão = sciutils.confusion_from_mask(map_class, classes, K)
    confusão0 = sciutils.confusion_from_mask(map0_class, classes, K)

    end_02 = timeit.default_timer()
    print('done in {} seg'.format(end_02 - start_02))

    print('post err min    = \n{}'.format(im_posteriori_error.min()))
    print('post err max    = \n{}'.format(im_posteriori_error.max()))
    print('')
    print('hit rate        = \n{}'.format(hitrate))
    print('hit rate eucl   = \n{}'.format(hitrate0))
    print('confusion       = \n{}'.format(confusão))
    print('confusion eucl  = \n{}'.format(confusão0))

    fig, axes = plt.subplots(1, 1, squeeze=False, figsize=(15, 6))
    cax = axes[0,0].imshow(im_posteriori_error[::-1,:],
                           extent=(xl[0],xl[1],yl[0],yl[1]),
                           cmap=plt.cm.viridis)
    axes[0,0].plot(mu[:,0], mu[:,1], 'rx', markersize=10, markeredgewidth=4)
    fig.colorbar(cax)
    axes[0,0].set_aspect('equal')
    sciutils.plot_finalize(plt, sim_dir / ('poster_error.eps'), show=show_plots)

    poster_colors = np.exp(im_posteriori)[...,np.newaxis] * \
        class_colors[:,np.newaxis,np.newaxis,:3]
    poster_colors = poster_colors.sum(axis=0)
    _, axes = plt.subplots(1, 1, squeeze=False, figsize=(15, 6))
    axes[0,0].imshow(poster_colors[::-1,:,:], extent=(xl[0],xl[1],yl[0],yl[1]))
    axes[0,0].plot(mu[:,0], mu[:,1], 'rx', markersize=10, markeredgewidth=4)
    axes[0,0].set_title(
        r'Posteriori---$p\left(\mathcal C_k\,\middle|\,\mathbf x\right)$')
    axes[0,0].set_aspect('equal')
    axes[0,0].xaxis.set_major_locator(
        mpl.ticker.MaxNLocator(integer=True))
    axes[0,0].yaxis.set_major_locator(
        mpl.ticker.MaxNLocator(integer=True))
    sciutils.plot_finalize(plt, sim_dir / ('poster.eps'), show=show_plots)

    fig, axes = plt.subplots(1, 1, squeeze=False, figsize=(15, 6))
    axes[0,0].imshow(im_map0_class[::-1,:],
                     extent=(xl[0],xl[1],yl[0],yl[1]),
                     cmap=class_cm)
    for k, cl in enumerate(class_colors):
        mask = (classes == k)
        axes[0,0].plot(pontos[mask,0], pontos[mask,1], 'o',
                       color=cl, mew=1, mec='k')
    axes[0,0].plot(mu[:,0], mu[:,1], 'rx', markersize=10, markeredgewidth=4)
    axes[0,0].set_title(r'Classification result (Euclidean)')
    axes[0,0].set_aspect('equal')
    axes[0,0].xaxis.set_major_locator(
        mpl.ticker.MaxNLocator(integer=True))
    axes[0,0].yaxis.set_major_locator(
        mpl.ticker.MaxNLocator(integer=True))
    sciutils.plot_finalize(plt, sim_dir / ('classifier0.eps'), show=show_plots)

    fig, axes = plt.subplots(1, 1, squeeze=False, figsize=(15, 6))
    axes[0,0].imshow(im_map_class[::-1,:],
                     extent=(xl[0],xl[1],yl[0],yl[1]),
                     cmap=class_cm)
    for k, cl in enumerate(class_colors):
        mask = (classes == k)
        axes[0,0].plot(pontos[mask,0], pontos[mask,1], 'o',
                       color=cl, mew=1, mec='k')
    axes[0,0].plot(mu[:,0], mu[:,1], 'rx', markersize=10, markeredgewidth=4)
    axes[0,0].set_title(r'Classification result')
    axes[0,0].set_aspect('equal')
    axes[0,0].xaxis.set_major_locator(
        mpl.ticker.MaxNLocator(integer=True))
    axes[0,0].yaxis.set_major_locator(
        mpl.ticker.MaxNLocator(integer=True))
    sciutils.plot_finalize(plt, sim_dir / ('classifier.eps'), show=show_plots)

    print()

    end_time = timeit.default_timer()
    total_time = end_time - start_time
    print('Tempo de execução total: {} seg'.format(total_time))

    if save:
        # Variáveis para serem salvas
        variáveis = {
            'Sigma':                ...,
            'mu':                   ...,

            'l':                    ...,
            'U':                    ...,
            'L12':                  ...,
            'UL12':                 ...,

            'total_time':           ...,
        }
        shelve_helper.save_in_shelve(variáveis, sim_dir, vars(), shelve_title)

def main_wrapper(args):
    kwargs = {
        'shelve_title': args.title,
        'show_plots': args.show_plots,
    }
    for x in ['sigma', 'mu', 'popsize', 'ellipses', 'bgres']:
        if getattr(args, x) is not None:
            kwargs[x] = getattr(args, x)
    main(**kwargs)

if __name__ == '__main__':

    parser = argparse.ArgumentParser(
        description="Lista 02, exercício 14")
    parser.add_argument(
        "title",
        help="Título da shelve que será usada para guardar os"
             " resultados do programa.",
        nargs='?',
        metavar="TITLE",
    )
    parser.add_argument(
        "-o", "--open",
        help="Abrir uma shelve.",
        nargs='?',
        metavar="FILE",
        const=Ellipsis,
    )
    parser.add_argument(
        "-s", "--show-plots",
        help="Mostrar os plots, além de gerar as figuras.",
        action="store_true",
    )
    parser.add_argument(
        "--ellipses",
        help="Tamanhos das elipses no contour plot.",
        type=str,
        default="0.1, 0.2, 0.5, 1.0")
    parser.add_argument(
        "--sigma",
        help="Covariance matrix.",
        type=str,
        default='1, 0, 0, 1')
    parser.add_argument(
        "--mu",
        help="Class centers",
        type=str,
        default='1, 1, 12, 8, 16, 1')
    parser.add_argument(
        "--popsize",
        help="Population size",
        type=int,
        default=1000)
    parser.add_argument(
        "--bgres",
        help="Background image resolution.",
        type=int,
        default=100)
    args = parser.parse_args()

    result = shelve_helper.process_args(args, main_wrapper)

    if result[0] is shelve_helper.Action.PROMPT:
        filename = result[1]
        for k in result[2]:
            globals()[k] = result[2][k]
        os.environ['PYTHONINSPECT'] = '1'
