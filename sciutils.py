#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""Pedro Angelo Medeiros Fonini <pedro.fonini@smt.ufrj.br>

"""

import sys
import re
import string
import subprocess
import inspect
import math

import numpy as np
import scipy
from scipy import linalg
from scipy import special

class LaTeXFormatter(string.Formatter):
    def format_field(self, value, format_spec):
        formatted_str = super().format_field(value, format_spec)
        if isinstance(value, float) and 'e' in formatted_str.lower():
            f, e = formatted_str.lower().split('e')
            return r'{}\times 10^{{{}}}'.format(f, int(e))
        return formatted_str
latex_formatter = LaTeXFormatter()

# from https://stackoverflow.com/questions/16259923/how-can-i-escape-latex-special-characters-inside-django-templates#25875504
def tex_escape(text):
    """
        :param text: a plain text message
        :return: the message escaped to appear correctly in LaTeX
    """
    conv = {
        '&': r'\&',
        '%': r'\%',
        '$': r'\$',
        '#': r'\#',
        '_': r'\_',
        '{': r'\{',
        '}': r'\}',
        '~': r'\textasciitilde{}',
        '^': r'\^{}',
        '\\': r'\textbackslash{}',
        '<': r'\textless',
        '>': r'\textgreater',
        u'\u03c6': r'$\varphi$',
    }
    regex = re.compile('|'.join(
        re.escape(unicode(key))
        for key in sorted(conv.keys(), key = lambda item: - len(item))
    ))
    return regex.sub(lambda match: conv[match.group()], text)


def print_raw(*args, **kwargs):
    print(*args, end='', **kwargs)
    if 'file' not in kwargs:
        f = sys.stdout
    else:
        f = kwargs['file']
    if f is None:
        f = sys.stdout
    f.flush()


def plot_finalize(plt, filepath,
                  tight=True, show=False, close=True,
                  epstopdf=True, pdfcrop=True):
    if tight:
        plt.tight_layout()
    plt.savefig(str(filepath))
    if show:
        plt.show()
    if close:
        plt.close('all')
    if epstopdf:
        subprocess.run(['epstopdf', str(filepath)], check=True,
                       stdout=subprocess.PIPE)
    filepath = filepath.with_suffix('.pdf')
    if pdfcrop:
        subprocess.run(['pdfcrop', str(filepath)], check=True,
                       stdout=subprocess.PIPE)

def printvar(varname, fmt="{}"):
    varvalue = inspect.currentframe().f_back.f_locals[varname]
    fmtfull = "{{}} = {}".format(fmt)
    print(fmtfull.format(varname, varvalue))


faint_gray = '#c0c0c0'

def normrange(x, axis=None):
    x = x - np.min(x, axis=axis, keepdims=True)
    x = x / np.max(x, axis=axis, keepdims=True)
    return x

def get_twoclass_lin_discriminant(mu, *, Sigma=None, priori=None, L=None):
    """Get the minimum-loss linear discriminant for two Gaussian classes

    Loss is minimum for class 1 if and only if:

        w^T x + w0 > 0

    where:

        w^T = (mu1 - mu0)^T Sigma^-1 ;

                (L10 - L11) p1
        w0 = log────────────── + 0.5*(Sigma^-1 mu0^2 - Sigma^-1 mu1^2) .
                (L01 - L00) p0

    'mu': 2xD array with the means mu[0] and mu[1] of the classes.
    'Sigma': DxD shared covariance matrix for the classes
    'L': 2x2 loss matrix, L_ki is the loss incurred by assigning an element
        of class k to class i
    'priori': (2,) vector of a priori log-probabilities.

    returns: (w, w0), where w is (D,) and w0 is a scalar.
    """
    mu = np.asarray(mu)
    assert len(mu.shape) == 2
    D = mu.shape[1]
    assert mu.shape == (2, D)
    if Sigma is None:
        Sigma = np.eye(D)
    Sigma = np.asarray(Sigma)
    assert Sigma.shape == (D, D)
    if priori is None:
        priori = scipy.log(np.array([0.5, 0.5]))
    priori = np.asarray(priori)
    assert priori.shape == (2,)
    if L is None:
        L = np.array([[0,1],[1,0]])
    L = np.asarray(L)
    assert L.shape == (2, 2)

    Smu = linalg.solve(Sigma, mu.T, assume_a='pos').T # Sigma^-1 * [ mu0 mu1 ]
    L = L[:,0] - L[:,1]; L[0] *= -1
    w0 = scipy.log(L) + priori
    w0 = w0[1] - w0[0]
    w0 += 0.5*(mu[0].dot(Smu[0]) - mu[1].dot(Smu[1]))
    w = Smu[1] - Smu[0]
    return w, w0

def line_cap_rect(w, w0, extent):
    """Calculate the intersection between a line and a rectangle.

    Given a rectangle extent=(x0, x1, y0, y1) and a line w.dot(x)+w0=0,
    return a list of points of intersection between the line and the border
    of the rectangle.
    """
    x0 = extent[0]
    x1 = extent[1]
    y0 = extent[2]
    y1 = extent[3]
    if w[0] == w[1] == 0:
        raise ZeroDivisionError("`w' can't be zero.")
    if w[1] == 0:
        x = -w0 / w[0]
        if x0 <= x <= x1:
            return [(x,y0), (x,y1)]
        else:
            return []
    if w[0] == 0:
        y = -w0 / w[1]
        if y0 <= y <= y1:
            return [(x0,y), (x1,y)]
        else:
            return []
    y  = -(w0 + w[0]*x0) / w[1]
    y_ = -(w0 + w[0]*x1) / w[1]
    x  = -(w0 + w[1]*y0) / w[0]
    x_ = -(w0 + w[1]*y1) / w[0]
    ix0 = (x ,y0)
    ix1 = (x_,y1)
    iy0 = (x0,y )
    iy1 = (x1,y_)
    if y0 <= y <= y1:
        if y0 <= y_ <= y1:
            return [iy0, iy1]
        elif y_ > y1:
            return [iy0, ix1]
        else: # y_ < y0
            return [iy0, ix0]
    elif y > y1:
        if y0 <= y_ <= y1:
            return [ix1, iy1]
        elif y_ > y1:
            return []
        else: # y_ < y0
            return [ix1, ix0]
    else: # y < y0
        if y0 <= y_ <= y1:
            return [ix0, iy1]
        elif y_ > y1:
            return [ix0, ix1]
        else: # y_ < y0
            return []

def shorten_segment(points, fractions):
    points = np.asarray(points)
    assert len(points.shape) >= 1
    assert points.shape[0] == 2
    fractions = np.asarray(fractions)
    assert len(fractions.shape) == 1
    assert fractions.shape[0] == 2

    A = points[0]
    B = points[1]
    D = B - A
    A = A + fractions[0]*D
    B = B - fractions[1]*D
    return np.stack((A, B))

def gauss_class_conditionals(x, S, m, axis=-1, log=False):
    """Given array 'x' of points, return class-conditional prob. densities

    D -> dimensionality of the input space
    K -> number of classes

    'x': must have length D along 'axis'.
    'S': K x D x D -> list of K covariance matrices. If K==1, broadcast.
    'm': K x D -> list of the mean vectors for each of the K classes.

    returns: same shape as 'x', except that at dimension 'axis' the
        length is K. For each point in 'x', return a K-vector with the
        class-conditional probabilities p(x|C_k).

    if 'log' is True, return log(p(x|C_k))
    """

    D = x.shape[axis]
    K = m.shape[0]

    _, logdets = np.linalg.slogdet(S)
    factors = -0.5*( D*np.log(math.tau) + logdets )
    S = np.linalg.inv(S) # scipy.linalg.* don't broadcast

    if len(S.shape) == 2:
        assert S.shape == (D,D)
        S = np.broadcast_to(S, (K, D, D))
    assert S.shape == (K, D, D)
    assert m.shape == (K, D)

    shape = list(x.shape)
    shape[axis] = K
    result = np.empty(tuple(shape))

    shape_iter = shape[:]
    del shape_iter[axis]
    shape_iter = tuple(shape_iter)

    # Removing elements from the middle of a list breaks negative
    # indexes when we want to use those indexes to re-insert that
    # element later on.
    if axis < 0:
        axis = len(shape) - axis

    for idx in np.ndindex(shape_iter):

        idx_full = list(idx)
        idx_full.insert(axis, slice(None))
        idx_full = tuple(idx_full)
        # Now, x[idx_full] is the D-dimensional vector x[???, :, ???]

        # quadratic form: (1/2) S_k^-1 (x-mu_k)^2
        displacement = x[idx_full] - m
        quadratic = np.empty((K, D))
        for k in range(K):
            quadratic[k] = S[k].dot(displacement[k])
        quadratic = 0.5 * (displacement*quadratic).sum(axis=-1)

        # density: (2pi)^(-D/2) * |S|^(-1/2) * exp{- 0.5 * S^-1 * (x-mu)^2}
        #          \------------v----------/         \---------v---------/
        #                   "factors"        * exp{-      "quadratic"     }
        result[idx_full] = factors - quadratic

    if not log:
        result = np.exp(result)
    return result

def joint_from_ccond(ccond, priori, axis=-1, log=False):
    """Get joint prob/density from class-conditional prob/density

    p(x,C_k) = p(x|C_k) * p(C_k)

    'ccond': dimension 'K' along 'axis' -> class-conditional probs
    'priori': rank-1 array of length K -> a priori probabilities

    returns: same shape as 'ccond', but at index [...,k,...] we will
        have p(x[...,...]|C_k)*p(C_k) instead of p(x[...,...],C_k).

    if 'log' is True, return log(p(x,C_k)), and assume that inputs are
        also log-probabilities.
    """

    ccond = np.asarray(ccond)
    priori = np.asarray(priori)
    assert len(priori.shape) == 1
    K = priori.shape[0]
    assert ccond.shape[axis] == K
    if axis < 0:
        assert axis >= -len(ccond.shape)
        axis = len(ccond.shape) + axis
    assert axis < len(ccond.shape)

    shape = tuple(1 if k!=axis else K for k in range(len(ccond.shape)))
    priori_resh = np.reshape(priori, shape)
    if not log:
        return ccond * priori_resh
    else:
        return ccond + priori_resh


def get_gauss_posteriori(X, mu, *, Sigma=None, priori=None, axis=-1):

    X = np.asarray(X)
    assert len(X.shape) >= 1
    D = X.shape[axis]

    mu = np.asarray(mu)
    assert len(mu.shape) == 2
    K = mu.shape[0]
    assert mu.shape == (K,D)

    if Sigma is None:
        Sigma = np.eye(D)
    Sigma = np.asarray(Sigma)
    if len(Sigma.shape) == 2:
        assert Sigma.shape == (D,D)
    else:
        assert len(Sigma.shape) == 3
        assert Sigma.shape == (K,D,D)

    if priori is None:
        priori = np.full((K,), -scipy.log(K))
    priori = np.asarray(priori)
    assert priori.shape == (K,)
    assert np.allclose(special.logsumexp(priori), 0)

    conds = gauss_class_conditionals(X, Sigma, mu, axis=axis, log=True)
    joint = joint_from_ccond(conds, priori, axis=axis, log=True)
    marginal = special.logsumexp(joint, axis=axis, keepdims=True)
    return joint - marginal

def confusion(classified_points):
    K = len(classified_points)
    C = np.empty((K,K))
    for k in range(K):
        C[k] = np.bincount(classified_points[k], minlength=K)
    return C

def confusion_from_mask(classified_points, classes, K):
    classified_points = np.asarray(classified_points)
    classes = np.asarray(classes)
    C = np.empty((K,K))
    for k in range(K):
        C[k] = np.bincount(classified_points[classes==k], minlength=K)
    return C
