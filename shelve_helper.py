#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""Pedro Angelo Medeiros Fonini <pedro.fonini@smt.ufrj.br>

Shelve management utilities
"""

import os
import pathlib
import glob
import sys
import enum
import textwrap
import shelve
import datetime

class GlobTimestamp:
    def __init__(self, string):
        self.string = string
    def __repr__(self):
        return "GlobTimestamp({!r})".format(self.string)
    def glob(self):
        result = ''
        it = iter(self.string)
        while True:
            try:
                c = next(it)
            except StopIteration:
                break
            if c == '%':
                try:
                    c = next(it)
                except StopIteration:
                    raise ValueError(
                        'GlobTimestamp: cannot end in unmatched ‘%’.')
                if   c == 'w': result += '[0-6]'
                elif c == 'd': result += '[0-3][0-9]'
                elif c == 'b': result += '[A-Z][a-z][a-z]'
                elif c == 'd': result += '[0-3][0-9]'
                elif c == 'm': result += '[0-1][0-9]'
                elif c == 'y': result += '[0-9][0-9]'
                elif c == 'Y': result += '[0-9][0-9][0-9][0-9]'
                elif c == 'H': result += '[0-2][0-9]'
                elif c == 'I': result += '[0-1][0-9]'
                elif c == 'M': result += '[0-5][0-9]'
                elif c == 'S': result += '[0-5][0-9]'
                elif c == 'f': result += '[0-9]'*6
                elif c == 'j': result += '[0-3][0-9][0-9]'
                elif c == 'U': result += '[0-5][0-9]'
                elif c == 'W': result += '[0-5][0-9]'
                elif c == '%': result += '%'
                else:
                    raise ValueError(
                        'GlobTimestamp: unknown directive ‘%{}’'.format(c))
            else:
                result += glob.escape(c)
        return result

globts = GlobTimestamp("%Y-%m-%d_%H:%M:%S.%f")

class GetShelveError(LookupError):
    pass

def get_shelves(search_in_dir='.', title=None):
    if title is not None:
        g = '**/shelve__{}'.format(glob.escape(title))
    else:
        g = '**/shelve__*'
    return (f for f in pathlib.Path(search_in_dir).glob(g) if not f.is_dir())

def get_last_shelve(search_in_dir='.'):
    most_recent_candidate = None
    most_recent_time = 0
    for f in get_shelves(search_in_dir):
        this_time = f.stat().st_mtime
        if this_time > most_recent_time:
            most_recent_time = this_time
            most_recent_candidate = f
    if most_recent_candidate is not None:
        return most_recent_candidate
    else:
        raise GetShelveError(
            "There are no shelves in the searched directory.")

class Action(enum.Enum):
    NONE = 0
    PROMPT = 1

# TODO: argumento extra: dicionário 'exit_codes' dizendo qual o exit code
#       pra cada caso.
def process_args(args, main):
    tw = textwrap.TextWrapper(subsequent_indent=' '*6)
    if  args.open is not None  and  args.title is not None:
        msg = (
            "Escolha apenas uma das opções: ou executa o algoritmo (e"
            " salva os resultados na shelve) ou abre a shelve usando a"
            " flag '-o'."
        )
        print(tw.fill("ERRO: " + msg), file=sys.stderr)
        sys.exit(1)
    if args.open is None:
        main(args)
        return (Action.NONE,)
    # Aqui, args.title é None, e args.open não é None
    filename = args.open
    if filename is Ellipsis:
        print(tw.fill(
            "Buscando última shelve usada. O nome dela estará"
            " disponível na variável ‘filename’."
        ))
        filename = get_last_shelve()
    else:
        try:
            filename = pathlib.Path(args.open).resolve()
        except FileNotFoundError:
            # Tratar args.open como título
            candidates = get_shelves(title=args.open)
            if len(candidates) == 0:
                msg = "Não existe uma shelve com título ‘{}’.".format(
                    args.open)
                print(tw.fill("ERRO: " + msg), file=sys.stderr)
                sys.exit(2)
            elif len(candidates) > 1:
                msg = "Existe mais de uma shelve com título ‘{}’:".format(
                    args.open)
                print(tw.fill("ERRO: " + msg), file=sys.stderr)
                for c in candidates:
                    print('  > ' + c, file=sys.stderr)
                sys.exit(3)
            # here, len == 1
            filename = candidates[0]
    shelve_globals = {}
    with shelve.open(str(filename)) as shv:
        for k in shv:
            shelve_globals[k] = shv[k]
    return Action.PROMPT, filename, shelve_globals


def setup_save_dir(
        categoria,
        save_dir='save',
        sim_dir_fmt='sim_{timestamp}_{categoria}',
    ):
    timestamp = datetime.datetime.utcnow().strftime(globts.string)
    save_dir = pathlib.Path(save_dir)
    sim_dir = pathlib.Path(sim_dir_fmt.format(
        timestamp=timestamp,
        categoria=categoria,
    ))
    if not save_dir.is_dir():
        save_dir.mkdir()
    if (save_dir/sim_dir).is_dir():
        raise RuntimeError('{} shouldn\'t exist!!'.format(save_dir/sim_dir))
    (save_dir/sim_dir).mkdir()
    sim_dir = (save_dir/sim_dir).resolve()
    return sim_dir


def save_in_shelve(variáveis, sim_dir, context, shelve_title=None):
    if shelve_title is None:
        shelve_title = ''
    filename = 'shelve__' + shelve_title
    final_path = sim_dir / filename
    print("Salvando dados no arquivo:  {}".format(final_path))
    with shelve.open(str(final_path)) as shv:
        for k in variáveis:
            if variáveis[k] is Ellipsis:
                shv[k] = context[k]
            else:
                shv[k] = variáveis[k]
    print("Dados salvos. Fim.")
