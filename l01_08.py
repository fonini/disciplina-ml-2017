#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""Universidade Federal do Rio de Janeiro
Pedro Angelo Medeiros Fonini <pedro.fonini@smt.ufrj.br>
Machine Learning - 2017.3

Lista 01, Exercício 8
"""

import os
import argparse
import timeit

import numpy as np
import matplotlib
if "DISPLAY" not in os.environ:
    matplotlib.use('Agg')
matplotlib.rcParams['text.usetex'] = True
matplotlib.rcParams['text.latex.unicode'] = True
matplotlib.rcParams['font.family'] = 'serif'
matplotlib.rcParams['font.size'] = 20
import matplotlib.pyplot as plt

import shelve_helper
import sciutils


def is_iterable(obj):
    try:
        _ = iter(obj)
        return True
    except TypeError:
        return False


def f(x):
    return x**2

class Data:
    def __init__(self, points):
        # points = [[x0,x1], [y0,y1]]
        self.points = points
    def __repr__(self):
        return 'Data({!r})'.format(self.points)
    def __getitem__(self, key):
        return self.points[key]
    def __setitem__(self, key, value):
        self.points[key] = value
    @classmethod
    def get_random_data(cls):
        x = sample_input(2)
        return cls(np.stack((x, f(x))))

def sample_input(*shape):
    if len(shape) == 1 and is_iterable(shape[0]):
        shape = shape[0]
    return 2*np.random.rand(*shape)-1

def algorithm(data):
    result = np.zeros(2)
    result[1] = (data[1,1] - data[1,0]) / (data[0,1] - data[0,0])
    result[0] = data[1,0] - result[1]*data[0,0]
    return result

def apply_g(g, x):
    a = g.take(0, axis=0)
    b = g.take(1, axis=0)
    return a + b*x


def main(ensemble_size, plt_ensemble_size,
         show_plots=False, shelve_title=None, save=True):

    categoria = 'l01_ex8'
    sim_dir = shelve_helper.setup_save_dir(categoria)

    start_time = timeit.default_timer()

    ### Experiment #1:
    ### average g(x)
    sciutils.print_raw('Experiment #1 ... ')
    start_01 = timeit.default_timer()

    ensemble_g = np.zeros((2,ensemble_size))
    for n in range(ensemble_size):
        ensemble_g[:,n] = algorithm(Data.get_random_data())
    average_g = np.mean(ensemble_g, axis=1)

    end_01 = timeit.default_timer()
    print('done in {} seg'.format(end_01 - start_01))

    print('a = {}'.format(average_g[0]))
    print('b = {}'.format(average_g[1]))

    fig, axes = plt.subplots(1, 1, squeeze=False)
    for n in range(plt_ensemble_size):
        axes[0,0].plot([-1, 1],
                       [apply_g(ensemble_g[:,n], -1),
                        apply_g(ensemble_g[:,n], +1)],
                       color=sciutils.faint_gray)
    x = np.linspace(-1, 1, num=1001)
    axes[0,0].plot(x, f(x), label=r'$f\left(x\right)$')
    axes[0,0].plot([-1, 1], [0,0],
                   label=r'$\overline{g}\left(x\right)$')
    axes[0,0].plot([-1, 1],
                   [apply_g(average_g, -1),
                    apply_g(average_g, +1)],
                   label=r'$\left\langle g^{\left(\mathcal{D}\right)}'
                         r'\left(x\right)\right\rangle$')
    axes[0,0].set_ylim([-3, 1])
    axes[0,0].set_title(sciutils.latex_formatter.format(
        r'$\left\langle g^{{\left(\mathcal{{D}}\right)}}'
        r'\left(x\right)\right\rangle = '
        r'\left({:.3}\right) + \left({:.3}\right)x$',
        average_g[0], average_g[1]))
    axes[0,0].legend(loc='best')
    plt.savefig(str(sim_dir / ('exp1_avg_g.eps')))

    ymax = max(abs(apply_g(average_g, -1)), abs(apply_g(average_g, +1)))
    axes[0,0].set_ylim(2*ymax*np.array([-1, +1]))
    if show_plots: plt.show()
    plt.savefig(str(sim_dir / ('exp1_avg_g_zoom.eps')))
    plt.close('all')

    fig, axes = plt.subplots(2, 1, squeeze=False)
    axes[0,0].hist(ensemble_g[0,:], bins=50,
                   label=r'$a^{\left(\mathcal{D}\right)}$')
    axes[0,0].axvline(0, color='g', linewidth=2,
                      label=r'$\overline a$')
    axes[0,0].axvline(average_g[0], color='r', linewidth=2,
                      label=r'$\left\langle a^{\left(\mathcal{D}\right)}'
                            r'\right\rangle $')
    axes[0,0].legend(loc='best')
    axes[1,0].hist(ensemble_g[1,:], bins=50,
                   label=r'$b^{\left(\mathcal{D}\right)}$')
    axes[1,0].axvline(0, color='g', linewidth=2,
                      label=r'$\overline b$')
    axes[1,0].axvline(average_g[1], color='r', linewidth=2,
                      label=r'$\left\langle b^{\left(\mathcal{D}\right)}'
                            r'\right\rangle $')
    axes[1,0].legend(loc='best')
    if show_plots: plt.show()
    plt.savefig(str(sim_dir / ('exp1_avg_g_hist.eps')))
    plt.close('all')

    print()

    ### Experiment #2:
    ### average E_out
    sciutils.print_raw('Experiment #2 ... ')
    start_02 = timeit.default_timer()

    ensemble_g_02 = np.zeros((2,ensemble_size))
    for n in range(ensemble_size):
        ensemble_g_02[:,n] = algorithm(Data.get_random_data())
    x = sample_input(ensemble_size)
    ensemble_sqerr = (apply_g(ensemble_g_02, x) - f(x))**2
    average_sqerr = np.mean(ensemble_sqerr)

    end_02 = timeit.default_timer()
    print('done in {} seg'.format(end_02 - start_02))

    print('sqerr = {}'.format(average_sqerr))

    fig, axes = plt.subplots(1, 1, squeeze=False)
    for n in range(plt_ensemble_size):
        axes[0,0].plot([-1, 1],
                       [apply_g(ensemble_g_02[:,n], -1),
                        apply_g(ensemble_g_02[:,n], +1)],
                       color=sciutils.faint_gray)
    for n in range(plt_ensemble_size):
        axes[0,0].plot(x[n]*np.array([1,1]),
                       [f(x[n]), apply_g(ensemble_g_02[:,n], x[n])],
                       color='c')
    x_f = np.linspace(-1, 1, num=1001)
    axes[0,0].plot(x_f, f(x_f), label=r'$f\left(x\right)$')
    for n in range(plt_ensemble_size):
        axes[0,0].plot(x[n], apply_g(ensemble_g_02[:,n], x[n]), 'o',
                       color=sciutils.faint_gray)
    axes[0,0].set_ylim([-3, 1])
    axes[0,0].set_title(sciutils.latex_formatter.format(
        r'$\left\langle '
        r'E_{{\mbox{{out}}}}\left(g^{{\left(\mathcal{{D}}\right)}}\right)'
        r'\right\rangle = {:.6}$', average_sqerr))
    if show_plots: plt.show()
    plt.savefig(str(sim_dir / ('exp2_avg_Eout.eps')))
    plt.close('all')

    print()

    ### Experiment #3:
    ### average bias
    sciutils.print_raw('Experiment #3 ... ')
    start_03 = timeit.default_timer()

    ensemble_g_03 = np.zeros((2,ensemble_size))
    for n in range(ensemble_size):
        ensemble_g_03[:,n] = algorithm(Data.get_random_data())
    average_g_03 = np.mean(ensemble_g_03, axis=1)

    x = sample_input(ensemble_size)
    ensemble_bias = (apply_g(average_g_03, x) - f(x))**2
    average_bias = np.mean(ensemble_bias)

    end_03 = timeit.default_timer()
    print('done in {} seg'.format(end_03 - start_03))

    print('bias = {}'.format(average_bias))

    fig, axes = plt.subplots(1, 1, squeeze=False)
    x_f = np.linspace(-1, 1, num=1001)
    axes[0,0].fill_between(x_f, 0, f(x_f), color='#ffccff',
                           label=sciutils.latex_formatter.format(
                                r'$f\left(x\right)-\sqrt{{\mbox{{bias}}'
                                r'\left(x\right)}}$'))
    for n in range(plt_ensemble_size):
        axes[0,0].plot([-1, 1],
                       [apply_g(ensemble_g_03[:,n], -1),
                        apply_g(ensemble_g_03[:,n], +1)],
                       color=sciutils.faint_gray)
    for n in range(plt_ensemble_size):
        axes[0,0].plot(x[n]*np.array([1,1]),
                       [f(x[n]), apply_g(average_g_03, x[n])],
                       color='c', linewidth=2)
    x_f = np.linspace(-1, 1, num=1001)
    axes[0,0].plot(x_f, f(x_f), label=r'$f\left(x\right)$', linewidth=2)
    axes[0,0].plot([-1, 1],
                   [apply_g(average_g_03, -1),
                    apply_g(average_g_03, +1)],
                   color='r', linewidth=2,
                   label=r'$\left\langle g^{\left(\mathcal{D}\right)}'
                         r'\left(x\right)\right\rangle$')
    for n in range(plt_ensemble_size):
        axes[0,0].plot(x[n], apply_g(average_g_03, x[n]), 'o',
                       color='r')
    axes[0,0].set_ylim([-3, 1])
    axes[0,0].legend(loc='best')
    axes[0,0].set_title(sciutils.latex_formatter.format(
        r'$\left\langle \mbox{{bias}}\left(x\right)'
        r' \right\rangle = {:.6}$', average_bias))
    if show_plots: plt.show()
    plt.savefig(str(sim_dir / ('exp3_avg_bias.eps')))
    plt.close('all')

    print()

    ### Experiment #4:
    ### average variance
    sciutils.print_raw('Experiment #4 ... ')
    start_04 = timeit.default_timer()

    ensemble_g_04 = np.zeros((2,ensemble_size))
    for n in range(ensemble_size):
        ensemble_g_04[:,n] = algorithm(Data.get_random_data())
    average_g_04 = np.mean(ensemble_g_04, axis=1)

    ensemble_g_04_2 = np.zeros((2,ensemble_size))
    for n in range(ensemble_size):
        ensemble_g_04_2[:,n] = algorithm(Data.get_random_data())
    x = sample_input(ensemble_size)
    ensemble_var = (apply_g(ensemble_g_04_2, x) - apply_g(average_g_04, x))**2
    average_var = np.mean(ensemble_var)

    end_04 = timeit.default_timer()
    print('done in {} seg'.format(end_04 - start_04))

    print('var = {}'.format(average_var))

    fig, axes = plt.subplots(1, 1, squeeze=False)
    x_f = np.linspace(-1, 1, num=1001)
    var_x = np.sqrt(2/3*x_f**2 + 1/9)
    axes[0,0].fill_between(x_f, -var_x, var_x, color='#ffccff',
                           label=sciutils.latex_formatter.format(
                                r'$\overline g\left(x\right) \pm'
                                r'\sqrt{{\mbox{{var}} \left(x\right)}}$'))
    for n in range(plt_ensemble_size):
        axes[0,0].plot([-1, 1],
                       [apply_g(ensemble_g_04_2[:,n], -1),
                        apply_g(ensemble_g_04_2[:,n], +1)],
                       color=sciutils.faint_gray)
    for n in range(plt_ensemble_size):
        axes[0,0].plot(x[n]*np.array([1,1]),
                       [apply_g(ensemble_g_04_2[:,n], x[n]),
                        apply_g(average_g_04, x[n])],
                       color='c', linewidth=2)
    x_f = np.linspace(-1, 1, num=1001)
    axes[0,0].plot(x_f, f(x_f), label=r'$f\left(x\right)$', linewidth=2)
    axes[0,0].plot([-1, 1],
                   [apply_g(average_g_04, -1),
                    apply_g(average_g_04, +1)],
                   color='r', linewidth=2,
                   label=r'$\left\langle g^{\left(\mathcal{D}\right)}'
                         r'\left(x\right)\right\rangle$')
    for n in range(plt_ensemble_size):
        axes[0,0].plot(x[n], apply_g(average_g_04, x[n]), 'o',
                       color='r')
        axes[0,0].plot(x[n], apply_g(ensemble_g_04_2[:,n], x[n]), 'o',
                       color=sciutils.faint_gray)
    axes[0,0].set_ylim([-3, 1])
    axes[0,0].legend(loc='best')
    axes[0,0].set_title(sciutils.latex_formatter.format(
        r'$\left\langle \mbox{{var}}\left(x\right)'
        r' \right\rangle = {:.6}$', average_var))
    if show_plots: plt.show()
    plt.savefig(str(sim_dir / ('exp4_avg_var.eps')))
    plt.close('all')

    fig, axes = plt.subplots(1, 1, squeeze=False)
    x_f = np.linspace(-1, 1, num=1001)
    axes[0,0].plot(x_f, f(x_f), label=r'$f\left(x\right)$')
    axes[0,0].plot(x_f, f(x_f), '--', linewidth=3,
                   label=r'$\sqrt{\mbox{bias}\left(x\right)}$')
    axes[0,0].plot(x_f, var_x, '--', linewidth=3,
                   label=r'$\sqrt{\mbox{var}\left(x\right)}$')
    axes[0,0].plot(x_f, np.sqrt(var_x**2 + f(x_f)**2), ':', linewidth=3,
                   label=r'$\sqrt{\mbox{var}\left(x\right)'
                         r'+ \mbox{bias}\left(x\right)}$')
    axes[0,0].legend()
    if show_plots: plt.show()
    plt.savefig(str(sim_dir / ('all_together_now.eps')))
    plt.close('all')

    print()

    end_time = timeit.default_timer()
    total_time = end_time - start_time
    print('Tempo de execução total: {} seg'.format(total_time))

    if save:
        # Variáveis para serem salvas
        variáveis = {
            'ensemble_size':        ...,

            'ensemble_g':           ...,
            'average_g':            ...,

            'ensemble_g_02':        ...,
            'ensemble_sqerr':       ...,
            'average_sqerr':        ...,

            'ensemble_g_03':        ...,
            'average_g_03':         ...,
            'ensemble_bias':        ...,
            'average_bias':         ...,

            'ensemble_g_04':        ...,
            'average_g_04':         ...,
            'ensemble_g_04_2':      ...,
            'ensemble_var':         ...,
            'average_var':          ...,

            'total_time':           ...,
        }
        shelve_helper.save_in_shelve(variáveis, sim_dir, vars(), shelve_title)

def main_wrapper(args):
    kwargs = {
        'shelve_title': args.title,
        'show_plots': args.show_plots,
    }
    for x in ['ensemble_size', 'plt_ensemble_size']:
        if getattr(args, x) is not None:
            kwargs[x] = getattr(args, x)
    main(**kwargs)

if __name__ == '__main__':

    parser = argparse.ArgumentParser(
        description="Lista 01, exercício 08 (Abu-Mostafa, Prob. 2.24)")
    parser.add_argument(
        "title",
        help="Título da shelve que será usada para guardar os"
             " resultados do programa.",
        nargs='?',
        metavar="TITLE",
    )
    parser.add_argument(
        "-o", "--open",
        help="Abrir uma shelve.",
        nargs='?',
        metavar="FILE",
        const=Ellipsis,
    )
    parser.add_argument(
        "--show-plots",
        help="Mostrar os plots, além de gerar as figuras.",
        action="store_true",
    )
    parser.add_argument(
        "--ensemble-size",
        help="Quantidade de repetições (Monte Carlo)",
        type=int,
        default=1e5,
    )
    parser.add_argument(
        "--plt-ensemble-size",
        help="Tamanhos dos ensembles plotados",
        type=int,
        default=20,
    )
    args = parser.parse_args()

    result = shelve_helper.process_args(args, main_wrapper)

    if result[0] is shelve_helper.Action.PROMPT:
        filename = result[1]
        for k in result[2]:
            globals()[k] = result[2][k]
        os.environ['PYTHONINSPECT'] = '1'
