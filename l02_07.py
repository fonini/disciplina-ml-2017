#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""Universidade Federal do Rio de Janeiro
Pedro Angelo Medeiros Fonini <pedro.fonini@smt.ufrj.br>
Machine Learning - 2017.3

Lista 02, Exercício 7
"""

import os
import argparse
import timeit

import numpy as np
import matplotlib
if "DISPLAY" not in os.environ:
    matplotlib.use('Agg')
matplotlib.rcParams['text.usetex'] = True
matplotlib.rcParams['text.latex.unicode'] = True
matplotlib.rcParams['font.family'] = 'serif'
matplotlib.rcParams['font.size'] = 20
import matplotlib.pyplot as plt

import shelve_helper
import sciutils


def mahalanobis(S, x, m):
    """Mahalanobis distance, squared.
    Returns (x - m)^T S (x - m) ."""
    x = x - m
    return np.sum(x * np.linalg.solve(S, x.T).T, axis=-1)

def main(
         show_plots=False, shelve_title=None, save=True):

    categoria = 'l02_ex7'
    sim_dir = shelve_helper.setup_save_dir(categoria)

    start_time = timeit.default_timer()

    Sigma = np.array([[ 0.3,  0.1,  0.1],
                      [ 0.1,  0.3, -0.1],
                      [ 0.1, -0.1,  0.3]])
    mu = np.array([[0.0, 0.0, 0.0],
                   [0.5, 0.5, 0.5]])

    ### Calculate parameters
    sciutils.print_raw('Calculate parameters ... ')
    start_01 = timeit.default_timer()

    Sw0 = np.linalg.solve(Sigma, mu[0])
    Sw1 = np.linalg.solve(Sigma, mu[1])
    w = Sw1 - Sw0
    w0 = -1/2*(mu[1].dot(Sw1) - mu[0].dot(Sw0))

    end_01 = timeit.default_timer()
    print('done in {} seg'.format(end_01 - start_01))

    for varname in ('Sw0', 'Sw1', 'w', 'w0'):
        sciutils.printvar(varname)

    print()

    end_time = timeit.default_timer()
    total_time = end_time - start_time
    print('Tempo de execução total: {} seg'.format(total_time))

    if save:
        # Variáveis para serem salvas
        variáveis = {
            'Sigma':                ...,
            'mu':                   ...,

            'Sw0':                  ...,
            'Sw0':                  ...,
            'w':                    ...,
            'w0':                   ...,

            'total_time':           ...,
        }
        shelve_helper.save_in_shelve(variáveis, sim_dir, vars(), shelve_title)

def main_wrapper(args):
    kwargs = {
        'shelve_title': args.title,
        'show_plots': args.show_plots,
    }
    for x in []:
        if getattr(args, x) is not None:
            kwargs[x] = getattr(args, x)
    main(**kwargs)

if __name__ == '__main__':

    parser = argparse.ArgumentParser(
        description="Lista 02, exercício 07")
    parser.add_argument(
        "title",
        help="Título da shelve que será usada para guardar os"
             " resultados do programa.",
        nargs='?',
        metavar="TITLE",
    )
    parser.add_argument(
        "-o", "--open",
        help="Abrir uma shelve.",
        nargs='?',
        metavar="FILE",
        const=Ellipsis,
    )
    parser.add_argument(
        "-s", "--show-plots",
        help="Mostrar os plots, além de gerar as figuras.",
        action="store_true",
    )
    args = parser.parse_args()

    result = shelve_helper.process_args(args, main_wrapper)

    if result[0] is shelve_helper.Action.PROMPT:
        filename = result[1]
        for k in result[2]:
            globals()[k] = result[2][k]
        os.environ['PYTHONINSPECT'] = '1'
