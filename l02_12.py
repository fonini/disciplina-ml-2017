#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""Universidade Federal do Rio de Janeiro
Pedro Angelo Medeiros Fonini <pedro.fonini@smt.ufrj.br>
Machine Learning - 2017.3

Lista 02, Exercício 12
"""

import os
import argparse
import timeit
import math

import numpy as np
import scipy
from scipy import special
import matplotlib
if "DISPLAY" not in os.environ:
    matplotlib.use('Agg')
matplotlib.rcParams['text.usetex'] = True
matplotlib.rcParams['text.latex.unicode'] = True
matplotlib.rcParams['font.family'] = 'serif'
matplotlib.rcParams['font.size'] = 20
import matplotlib.pyplot as plt

import shelve_helper
import sciutils

def main(ellipses, qtdperclass, bgres,
         show_plots=False, shelve_title=None, save=True):

    categoria = 'l02_ex12'
    sim_dir = shelve_helper.setup_save_dir(categoria)

    start_time = timeit.default_timer()

    Sigma = np.array([[0.2, 0.0],
                      [0.0, 0.2]])
    mu = np.array([[ 1.0, 1.0],
                   [ 1.5, 1.5]])
    prioris = scipy.log(np.array([0.5, 0.5]))
    L = np.array([[.0, 1],
                  [.5, 0]])

    class_colors = plt.cm.viridis([0., 1.])

    ### Ellipse axes
    sciutils.print_raw('Show ellipses ... ')
    start_02 = timeit.default_timer()

    l, U = np.linalg.eigh(Sigma)
    L12 = np.diag(l**(1/2))
    UL12 = U.dot(L12) # U*Lambda^{1/2}

    end_02 = timeit.default_timer()
    print('done in {} seg'.format(end_02 - start_02))

    print('l    = \n{}'.format(l))
    print('U    = \n{}'.format(U))
    print('L12  = \n{}'.format(L12))
    print('UL12 = \n{}'.format(UL12))

    el_sizes = [float(x) for x in ellipses.split(',')]
    num_elps = len(el_sizes)
    colors = plt.cm.viridis(np.linspace(0, 0.8, num_elps))

    fig, axes = plt.subplots(1, 1, squeeze=False, figsize=(15, 6))
    t = np.linspace(0, 2*np.pi, num=1001)
    y = np.stack((np.cos(t), np.sin(t)), axis=1)
    y = UL12.dot(y.T).T
    for muidx in range(mu.shape[0]):
        for i in range(num_elps):
            y_scaled = mu[muidx] + el_sizes[i] * y
            axes[0,0].plot(y_scaled[:,0], y_scaled[:,1],
                color=colors[i], linewidth=2)
    axes[0,0].plot(mu[:,0], mu[:,1], 'x', markersize=10, markeredgewidth=4,
        label=sciutils.latex_formatter.format(r'Class centers'))
    box = axes[0,0].get_position()
    axes[0,0].set_position([box.x0, box.y0, box.width * 0.8, box.height])
    axes[0,0].set_aspect('equal')
    axes[0,0].xaxis.set_major_locator(
        matplotlib.ticker.MaxNLocator(integer=True))
    axes[0,0].yaxis.set_major_locator(
        matplotlib.ticker.MaxNLocator(integer=True))
    axes[0,0].legend(loc='center left', bbox_to_anchor=(1, 0.5))
    axes[0,0].grid(True)
    sciutils.plot_finalize(plt, sim_dir / ('contour.eps'), show=show_plots)

    print()

    ### Classify points
    sciutils.print_raw('Classify points ... ')
    start_02 = timeit.default_timer()

    pontos = np.stack(
        np.random.standard_normal((qtdperclass, 2)).dot(UL12.T) + m
        for m in mu)

    _, ax = plt.subplots(1, 1, squeeze=True, figsize=(15, 6))
    for color, pts in zip(class_colors, pontos):
        ax.plot(pts[:,0], pts[:,1], '.', color=color)
    xl = ax.get_xlim()
    yl = ax.get_ylim()
    range_min = min((xl[0], yl[0]))
    range_max = max((xl[1], yl[1]))
    plt.close('all')

    xy = np.stack(np.meshgrid(np.linspace(xl[0],xl[1],bgres),
                              np.linspace(yl[0],yl[1],bgres)))
    im_posteriori = sciutils.get_gauss_posteriori(
        xy, mu, Sigma=Sigma, priori=prioris, axis=0)
    im_posteriori_error = special.logsumexp(im_posteriori, axis=0)
    im_xpec_loss0 = np.tensordot(np.array([[0,1],[1,0]]),
                                 scipy.exp(im_posteriori),
                                 [(0,), (0,)])
    im_xpec_loss = np.tensordot(L,
                                scipy.exp(im_posteriori),
                                [(0,), (0,)])

    posteriori = sciutils.get_gauss_posteriori(
        pontos, mu, Sigma=Sigma, priori=prioris)
    xpec_loss = np.tensordot(L, scipy.exp(posteriori), [(0,), (2,)])
    min_loss_class = np.argmin(xpec_loss, axis=0)
    hits = min_loss_class == np.arange(2).reshape(
        tuple(1 if k!=0 else 2 for k in range(len(min_loss_class.shape))))
    hitrate = hits.mean()
    confusão = sciutils.confusion(min_loss_class)
    loss = L * confusão

    w_LI, w0_LI = sciutils.get_twoclass_lin_discriminant(
        mu, Sigma=Sigma, priori=prioris)
    A_LI, B_LI = sciutils.line_cap_rect(w_LI, w0_LI,
                                        extent=(xl[0],xl[1],yl[0],yl[1]))
    A_LI, B_LI = sciutils.shorten_segment((A_LI, B_LI), (0.03, 0.03))

    w, w0 = sciutils.get_twoclass_lin_discriminant(
        mu, Sigma=Sigma, priori=prioris, L=L)
    A, B = sciutils.line_cap_rect(w, w0,
                                  extent=(xl[0],xl[1],yl[0],yl[1]))
    A, B = sciutils.shorten_segment((A, B), (0.03, 0.03))

    end_02 = timeit.default_timer()
    print('done in {} seg'.format(end_02 - start_02))

    print('post err min    = \n{}'.format(im_posteriori_error.min()))
    print('post err max    = \n{}'.format(im_posteriori_error.max()))
    print('w               = \n{}'.format(w))
    print('w0              = \n{}'.format(w0))
    print('')
    print('hit rate        = \n{}'.format(hitrate))
    print('confusion       = \n{}'.format(confusão))
    print('loss            = \n{}'.format(loss))
    print('total loss      = \n{}'.format(loss.sum()))

    fig, axes = plt.subplots(1, 1, squeeze=False, figsize=(15, 6))
    cax = axes[0,0].imshow(im_posteriori_error[::-1,:],
                           extent=(xl[0],xl[1],yl[0],yl[1]),
                           cmap=plt.cm.viridis)
    axes[0,0].plot(mu[:,0], mu[:,1], 'rx', markersize=10, markeredgewidth=4)
    axes[0,0].plot([A_LI[0], B_LI[0]], [A_LI[1], B_LI[1]], color='k')
    fig.colorbar(cax)
    axes[0,0].set_aspect('equal')
    sciutils.plot_finalize(plt, sim_dir / ('poster_error.eps'), show=show_plots)

    poster_colors = np.exp(im_posteriori)[...,np.newaxis] * \
        class_colors[:,np.newaxis,np.newaxis,:3]
    poster_colors = poster_colors.sum(axis=0)
    fig, axes = plt.subplots(1, 1, squeeze=False, figsize=(15, 6))
    axes[0,0].imshow(poster_colors[::-1,:,:], extent=(xl[0],xl[1],yl[0],yl[1]))
    axes[0,0].plot(mu[:,0], mu[:,1], 'rx', markersize=10, markeredgewidth=4)
    axes[0,0].plot([A[0], B[0]], [A[1], B[1]], color='k')
    axes[0,0].plot([A_LI[0], B_LI[0]], [A_LI[1], B_LI[1]], color='k')
    axes[0,0].set_title(
        r'Posteriori---$p\left(\mathcal C_k\,\middle|\,\mathbf x\right)$')
    axes[0,0].set_aspect('equal')
    sciutils.plot_finalize(plt, sim_dir / ('poster.eps'), show=show_plots)

    xl_colors = im_xpec_loss0[...,np.newaxis] * \
        class_colors[:,np.newaxis,np.newaxis,:3]
    xl_colors = xl_colors.sum(axis=0)
    fig, axes = plt.subplots(1, 1, squeeze=False, figsize=(15, 6))
    axes[0,0].imshow(sciutils.normrange(xl_colors[::-1,:,:]),
                     extent=(xl[0],xl[1],yl[0],yl[1]))
    axes[0,0].plot(mu[:,0], mu[:,1], 'rx', markersize=10, markeredgewidth=4)
    axes[0,0].plot([A_LI[0], B_LI[0]], [A_LI[1], B_LI[1]], color='k')
    for color, pts in zip(class_colors, pontos):
        axes[0,0].plot(pts[:,0], pts[:,1], 'o', color=color, mew=1, mec='k')
    axes[0,0].set_title(r'Expected loss ($L=1-I$)')
    axes[0,0].set_aspect('equal')
    sciutils.plot_finalize(plt, sim_dir / ('xl0.eps'), show=show_plots)

    fig, axes = plt.subplots(1, 1, squeeze=False, figsize=(15, 6))
    axes[0,0].imshow(np.argmin(im_xpec_loss0, axis=0)[::-1,:],
                     extent=(xl[0],xl[1],yl[0],yl[1]),
                     cmap=plt.cm.viridis)
    axes[0,0].plot(mu[:,0], mu[:,1], 'rx', markersize=10, markeredgewidth=4)
    axes[0,0].plot([A_LI[0], B_LI[0]], [A_LI[1], B_LI[1]], color='k')
    for color, pts in zip(class_colors, pontos):
        axes[0,0].plot(pts[:,0], pts[:,1], 'o', color=color, mew=1, mec='k')
    axes[0,0].set_title(r'Classification result ($L=1-I$)')
    axes[0,0].set_aspect('equal')
    sciutils.plot_finalize(plt, sim_dir / ('classifier.eps'), show=show_plots)

    xl_colors = im_xpec_loss[...,np.newaxis] * \
        class_colors[:,np.newaxis,np.newaxis,:3]
    xl_colors = xl_colors.sum(axis=0)
    fig, axes = plt.subplots(1, 1, squeeze=False, figsize=(15, 6))
    axes[0,0].imshow(sciutils.normrange(xl_colors[::-1,:,:]),
                     extent=(xl[0],xl[1],yl[0],yl[1]))
    axes[0,0].plot(mu[:,0], mu[:,1], 'rx', markersize=10, markeredgewidth=4)
    axes[0,0].plot([A[0], B[0]], [A[1], B[1]], color='k')
    for color, pts in zip(class_colors, pontos):
        axes[0,0].plot(pts[:,0], pts[:,1], 'o', color=color, mew=1, mec='k')
    axes[0,0].set_title(r'Expected loss')
    axes[0,0].set_aspect('equal')
    sciutils.plot_finalize(plt, sim_dir / ('xl.eps'), show=show_plots)

    fig, axes = plt.subplots(1, 1, squeeze=False, figsize=(15, 6))
    axes[0,0].imshow(np.argmin(im_xpec_loss, axis=0)[::-1,:],
                     extent=(xl[0],xl[1],yl[0],yl[1]),
                     cmap=plt.cm.viridis)
    axes[0,0].plot(mu[:,0], mu[:,1], 'rx', markersize=10, markeredgewidth=4)
    axes[0,0].plot([A[0], B[0]], [A[1], B[1]], color='k')
    for color, pts in zip(class_colors, pontos):
        axes[0,0].plot(pts[:,0], pts[:,1], 'o', color=color, mew=1, mec='k')
    axes[0,0].set_title(r'Classification result')
    axes[0,0].set_aspect('equal')
    sciutils.plot_finalize(plt, sim_dir / ('classifier.eps'), show=show_plots)

    print()

    end_time = timeit.default_timer()
    total_time = end_time - start_time
    print('Tempo de execução total: {} seg'.format(total_time))

    if save:
        # Variáveis para serem salvas
        variáveis = {
            'Sigma':                ...,
            'mu':                   ...,

            'l':                    ...,
            'U':                    ...,
            'L12':                  ...,
            'UL12':                 ...,

            'im_posteriori':        ...,
            'posteriori':           ...,
            'xpec_loss':            ...,
            'min_loss_class':       ...,
            'hits':                 ...,
            'hitrate':              ...,
            'confusão':             ...,
            'loss':                 ...,
            'w':                    ...,
            'w0':                   ...,

            'total_time':           ...,
        }
        shelve_helper.save_in_shelve(variáveis, sim_dir, vars(), shelve_title)

def main_wrapper(args):
    kwargs = {
        'shelve_title': args.title,
        'show_plots': args.show_plots,
    }
    for x in ['ellipses', 'qtdperclass', 'bgres']:
        if getattr(args, x) is not None:
            kwargs[x] = getattr(args, x)
    main(**kwargs)

if __name__ == '__main__':

    parser = argparse.ArgumentParser(
        description="Lista 02, exercício 12")
    parser.add_argument(
        "title",
        help="Título da shelve que será usada para guardar os"
             " resultados do programa.",
        nargs='?',
        metavar="TITLE",
    )
    parser.add_argument(
        "-o", "--open",
        help="Abrir uma shelve.",
        nargs='?',
        metavar="FILE",
        const=Ellipsis,
    )
    parser.add_argument(
        "-s", "--show-plots",
        help="Mostrar os plots, além de gerar as figuras.",
        action="store_true",
    )
    parser.add_argument(
        "--ellipses",
        help="Tamanhos das elipses no contour plot.",
        type=str,
        default="0.1, 0.2, 0.5, 1.0")
    parser.add_argument(
        "-N", "--qtdperclass",
        help="Quantidade de exemplos por classe.",
        type=int,
        default=100)
    parser.add_argument(
        "--bgres",
        help="Background image resolution.",
        type=int,
        default=100)
    args = parser.parse_args()

    result = shelve_helper.process_args(args, main_wrapper)

    if result[0] is shelve_helper.Action.PROMPT:
        filename = result[1]
        for k in result[2]:
            globals()[k] = result[2][k]
        os.environ['PYTHONINSPECT'] = '1'
