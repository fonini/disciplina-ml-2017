#!/usr/bin/python3
# -*- coding: utf-8 -*-

"""Universidade Federal do Rio de Janeiro
Pedro Angelo Medeiros Fonini <pedro.fonini@smt.ufrj.br>
Machine Learning - 2017.3

Lista 02, Exercício 6
"""

import os
import argparse
import timeit

import numpy as np
import matplotlib
if "DISPLAY" not in os.environ:
    matplotlib.use('Agg')
matplotlib.rcParams['text.usetex'] = True
matplotlib.rcParams['text.latex.unicode'] = True
matplotlib.rcParams['font.family'] = 'serif'
matplotlib.rcParams['font.size'] = 20
import matplotlib.pyplot as plt

import shelve_helper
import sciutils


def mahalanobis(S, x, m):
    """Mahalanobis distance, squared.
    Returns (x - m)^T S (x - m) ."""
    x = x - m
    return np.sum(x * np.linalg.solve(S, x.T).T, axis=-1)

def main(ellipses,
         show_plots=False, shelve_title=None, save=True):

    categoria = 'l02_ex6'
    sim_dir = shelve_helper.setup_save_dir(categoria)

    start_time = timeit.default_timer()

    Sigma = np.array([[1.2, 0.4],
                      [0.4, 1.8]])
    mu = np.array([[ 0.1, 0.1],
                   [ 2.1, 1.9],
                   [-1.5, 2.0]])
    x = np.array([1.6, 1.5])

    ### Mahalanobis distance to each center
    sciutils.print_raw('Calculate Mahalanobis distances ... ')
    start_01 = timeit.default_timer()

    maha_dists_sq = mahalanobis(Sigma, x, mu)

    end_01 = timeit.default_timer()
    print('done in {} seg'.format(end_01 - start_01))

    for i, dsq in enumerate(maha_dists_sq):
        print('Distance squared to mu_{}: {}'.format(i, dsq))

    print()

    ### Ellipse axes
    sciutils.print_raw('Experiment #2 ... ')
    start_02 = timeit.default_timer()

    l, U = np.linalg.eigh(Sigma)
    L12 = np.diag(l**(1/2))
    UL12 = U.dot(L12) # U*Lambda^{1/2}

    end_02 = timeit.default_timer()
    print('done in {} seg'.format(end_02 - start_02))

    print('l    = \n{}'.format(l))
    print('U    = \n{}'.format(U))
    print('L12  = \n{}'.format(L12))
    print('UL12 = \n{}'.format(UL12))

    el_sizes = [float(x) for x in ellipses.split(',')]
    num_elps = len(el_sizes)
    colors = plt.cm.viridis(np.linspace(0, 0.8, num_elps))

    fig, axes = plt.subplots(1, 1, squeeze=False, figsize=(15, 6))
    t = np.linspace(0, 2*np.pi, num=1001)
    y = np.stack((np.cos(t), np.sin(t)), axis=1)
    y = UL12.dot(y.T).T
    for i in range(num_elps):
        y_scaled = mu[1] + el_sizes[i] * y
        print('size {}: {}'.format(i, el_sizes[i]))
        axes[0,0].plot(y_scaled[:,0], y_scaled[:,1],
            color=colors[i], linewidth=2,
            label=sciutils.latex_formatter.format(r'$M={}$', el_sizes[i]))
    axes[0,0].plot(mu[:,0], mu[:,1], 'x', markersize=10, markeredgewidth=4,
        label=sciutils.latex_formatter.format(r'Class centers'))
    axes[0,0].plot(x[0], x[1], '+', markersize=10, markeredgewidth=4,
        label=sciutils.latex_formatter.format(r'$\mathbf x$'), color='r')
    axes[0,0].set_ylim([-.5, 3.5])
    axes[0,0].set_xlim([-2, 3.5])
    box = axes[0,0].get_position()
    axes[0,0].set_position([box.x0, box.y0, box.width * 0.8, box.height])
    axes[0,0].set_aspect('equal')
    axes[0,0].xaxis.set_major_locator(
        matplotlib.ticker.MaxNLocator(integer=True))
    axes[0,0].yaxis.set_major_locator(
        matplotlib.ticker.MaxNLocator(integer=True))
    axes[0,0].legend(loc='center left', bbox_to_anchor=(1, 0.5))
    axes[0,0].grid(True)
    sciutils.plot_finalize(plt, sim_dir / ('contour.eps'), show=show_plots)

    print()

    end_time = timeit.default_timer()
    total_time = end_time - start_time
    print('Tempo de execução total: {} seg'.format(total_time))

    if save:
        # Variáveis para serem salvas
        variáveis = {
            'Sigma':                ...,
            'mu':                   ...,
            'x':                    ...,
            'maha_dists_sq':        ...,

            'l':                    ...,
            'U':                    ...,
            'L12':                  ...,
            'UL12':                 ...,

            'total_time':           ...,
        }
        shelve_helper.save_in_shelve(variáveis, sim_dir, vars(), shelve_title)

def main_wrapper(args):
    kwargs = {
        'shelve_title': args.title,
        'show_plots': args.show_plots,
    }
    for x in ['ellipses']:
        if getattr(args, x) is not None:
            kwargs[x] = getattr(args, x)
    main(**kwargs)

if __name__ == '__main__':

    parser = argparse.ArgumentParser(
        description="Lista 02, exercício 06 (Theodoridis, Prob. 2.7)")
    parser.add_argument(
        "title",
        help="Título da shelve que será usada para guardar os"
             " resultados do programa.",
        nargs='?',
        metavar="TITLE",
    )
    parser.add_argument(
        "-o", "--open",
        help="Abrir uma shelve.",
        nargs='?',
        metavar="FILE",
        const=Ellipsis,
    )
    parser.add_argument(
        "-s", "--show-plots",
        help="Mostrar os plots, além de gerar as figuras.",
        action="store_true",
    )
    parser.add_argument(
        "--ellipses",
        help="Tamanhos das elipses no contour plot.",
        type=str,
        default="0.1, 0.2, 0.5, 1.0")
    args = parser.parse_args()

    result = shelve_helper.process_args(args, main_wrapper)

    if result[0] is shelve_helper.Action.PROMPT:
        filename = result[1]
        for k in result[2]:
            globals()[k] = result[2][k]
        os.environ['PYTHONINSPECT'] = '1'
